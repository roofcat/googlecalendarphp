<?php

class GoogleService 
{
	protected $client;
	protected $profile;
	protected $calendar;

	public function __construct(Google_Client $googleClient = null)
	{
		$this->client = $googleClient;

		if ($this->client)
		{
			$this->client->setClientId('874571669699-6kl6tjltu685mm9gnla796ig492194lm.apps.googleusercontent.com');
			$this->client->setClientSecret('dRPLynZyVcm97PzSfJzfaWhm');
			$this->client->setRedirectUri('http://localhost/calendar/index.php');
			$this->client->setScopes(array('https://www.googleapis.com/auth/userinfo.email',
									'https://www.googleapis.com/auth/userinfo.profile',
									'https://www.googleapis.com/auth/calendar'));

			$this->profile = new Google_Service_Oauth2($this->client);
			$this->calendar = new Google_Service_Calendar($this->client);
		}
	}

	public function isLoggedIn () 
	{
		return isset($_SESSION['access_token']);
	}

	public function getUser()
	{
		return $this->profile->userinfo->get();
	}

	public function getAuthUrl () 
	{
		return $this->client->createAuthUrl();
	}

	public function checkRedirectCode () 
	{
		if (isset($_GET['code'])) 
		{
			$this->client->authenticate($_GET['code']);

			$this->setToken($this->client->getAccessToken());
			
			return true;
		}
		return false;
	}

	public function setToken ($token) 
	{
		$_SESSION['access_token'] = $token;
		$_SESSION['userinfo'] = $this->getUser();
		$this->client->setAccessToken($token);
	}

	public function logout () 
	{
		unset($_SESSION['access_token']);
	}
}