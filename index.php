<?php

require_once 'app/init.php';

$googleClient = new Google_Client;
$auth = new GoogleService($googleClient);

if($auth->checkRedirectCode())
{
	header('Location: index.php');
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Google Calendar</title>
</head>
<body>
	<?php if (!$auth->isLoggedIn()): ?>
		<a href="<?php echo $auth->getAuthUrl(); ?>">Iniciar sesión</a>
	<?php else: ?>
		Bienvenido <?php print_r($_SESSION['userinfo']) ?> <a href="logout.php">Salir</a>
	<?php endif; ?>
</body>
</html>